using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrDoorScript : MonoBehaviour
{
    [SerializeField] private Material _activated;
    [SerializeField] private Material _disactivated;
    [SerializeField] private GameObject _inputOneIndicator;
    [SerializeField] private GameObject _inputTwoIndicator;
    
    //Output
    [SerializeField] private DoorManager _door;
    [SerializeField] private AndDoorScript _andDoor;
    [SerializeField] private BridgeController _bridge;
    [SerializeField] private LaserScript _laser;
    [SerializeField] private OrDoorScript _orDoor;
    [SerializeField] private ListActivatorScript _listActivator;
    [SerializeField] private int _outputType = 1;
    /*
     * 1 Door (Default)
     * 2 AndDoor
     * 3 Bridge
     * 4 Laser
     * 5 OrDoor
     * 6 ListActivator
     */

    private bool _activate;
    private bool _disactivate;

    private bool _inputOne;
    private bool _inputTwo;
    
    // Start is called before the first frame update
    void Start()
    {
        _activate = false;
        _disactivate = false;
        _inputOne = false;
        _inputTwo = false;
    }

    public void SetInputOn()
    {
        if (_inputOne)
        {
            _inputTwo = true;
            _inputTwoIndicator.GetComponent<MeshRenderer>().material = _activated;
        }

        else
        {
            _inputOne = true;
            _inputOneIndicator.GetComponent<MeshRenderer>().material = _activated;
            _activate = true;
            _disactivate = false;
        }
    }

    public void SetInputOff()
    {
        if (_inputTwo)
        {
            _inputTwo = false;
            _inputTwoIndicator.GetComponent<MeshRenderer>().material = _disactivated;
        }

        else
        {
            _inputOne = false;
            _inputOneIndicator.GetComponent<MeshRenderer>().material = _disactivated;
            _activate = false;
            _disactivate = true;
        }
    }
    
    // Update is called once per frame
    void Update()
    {
        if (_activate)
        {
            _activate = false;
            Activate();
        }

        if (_disactivate)
        {
            _disactivate = false;
            Disactivate();
        }
    }

    private void Activate()
    {
        if (_outputType == 1)
            _door.Open();
        else if (_outputType == 2)
            _andDoor.SetInputOn();
        else if (_outputType == 3)
            _bridge.Open();
        else if (_outputType == 4)
            _laser.Activate();
        else if (_outputType == 5)
            _orDoor.SetInputOn();
        else if (_outputType == 6)
            _listActivator.Activate();
    }

    private void Disactivate()
    {
        if (_outputType == 1)
            _door.Close();
        else if (_outputType == 2)
            _andDoor.SetInputOff();
        else if (_outputType == 3)
            _bridge.Close();
        else if (_outputType == 4)
            _laser.Desactivate();
        else if (_outputType == 5)
            _orDoor.SetInputOff();
        else if (_outputType == 6)
            _listActivator.Desactivate();
    }
}
