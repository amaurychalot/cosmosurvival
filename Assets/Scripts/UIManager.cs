using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private GameObject _playerOneUI;
    [SerializeField] private GameObject _playerTwoUI;
    [SerializeField] private Image _twoPlayersUISquare;
    [SerializeField] private Image playerOneUISquare;
    [SerializeField] private Image playerTwoUISquare;

    [SerializeField] private GameObject _playerOne;
    [SerializeField] private GameObject _playerTwo;
    
    private bool _isCentralSquareActive;
    private bool _isPlayerOneSquareActive;
    private bool _isPlayerTwoSquareActive;
    private bool _normalSetSquares;
    private int _nbBlinkCentralSquare;
    private int _nbBlinkPlayerOneSquare;
    private int _nbBlinkPlayerTwoSquare;
    private float _blinkCentralTimer;
    private float _blinkPlayerOneTimer;
    private float _blinkPlayerTwoTimer;

    private bool _isWarningPlayerOne = false;
    private bool _isWarningPlayerTwo = false;

    [SerializeField] private float _blinkingRate = 0.5f;
    [SerializeField] private int _blinkingDuration = 6;

    [SerializeField] private GameObject _camera;
    private void Start()
    {
        _twoPlayersUISquare.enabled = true;
        _playerOneUI.SetActive(false);
        _playerTwoUI.SetActive(false);
        _isCentralSquareActive = true;
        _isPlayerOneSquareActive = false;
        _isPlayerTwoSquareActive = false;
        _normalSetSquares = true;
        _nbBlinkCentralSquare = 0;
        _nbBlinkPlayerOneSquare = _blinkingDuration;
        _nbBlinkPlayerTwoSquare = _blinkingDuration;
        _blinkCentralTimer = 0;
        _blinkPlayerOneTimer = 0;
        _blinkPlayerTwoTimer = 0;
    }

    private void Update()
    {
        CheckPlayersPosition();
        CheckBlinkingSquares();
    }

    //S'occupe du clignotement des carres de la UI
    private void CheckBlinkingSquares()
    {
        if (_isCentralSquareActive && _nbBlinkCentralSquare < _blinkingDuration)
        {
            _blinkCentralTimer += Time.deltaTime;
            if (_blinkCentralTimer >= _blinkingRate)
            {
                _nbBlinkCentralSquare++;
                _blinkCentralTimer = 0;
                _twoPlayersUISquare.DOFade((_nbBlinkCentralSquare % 2 == 0 ? 0 : 1), _blinkingRate);
            }
        }

        if (_isPlayerOneSquareActive && _nbBlinkPlayerOneSquare < _blinkingDuration)
        {
            _blinkPlayerOneTimer += Time.deltaTime;
            if (_blinkPlayerOneTimer >= _blinkingRate)
            {
                _nbBlinkPlayerOneSquare++;
                _blinkPlayerOneTimer = 0;
                playerOneUISquare.DOFade((_nbBlinkPlayerOneSquare % 2 == 0 ? 0 : 1), _blinkingRate);
            }
        }

        if (_isPlayerTwoSquareActive && _nbBlinkPlayerTwoSquare < _blinkingDuration)
        {
            _blinkPlayerTwoTimer += Time.deltaTime;
            if (_blinkPlayerTwoTimer >= _blinkingRate)
            {
                _nbBlinkPlayerTwoSquare++;
                _blinkPlayerTwoTimer = 0;
                playerTwoUISquare.DOFade((_nbBlinkPlayerTwoSquare % 2 == 0 ? 0 : 1), _blinkingRate);
            }
        }
    }

    //Reset le nombre de clignotement des petites carres
    private void ResetBlinkingNbSquares()
    {
        playerOneUISquare.DOFade(1, 0);
        playerTwoUISquare.DOFade(1, 0);
        _nbBlinkPlayerOneSquare = 0;
        _nbBlinkPlayerTwoSquare = 0;
    }
    
    //Empeche la UI d'un des deux joueurs de géner l'autre joueur.
    private void CheckPlayersPosition()
    {
        if (_isPlayerOneSquareActive && !_isPlayerTwoSquareActive)
        {
            if (_playerTwo.transform.position.x < _camera.transform.position.x && _normalSetSquares)
            {
                _playerOneUI.transform.DOLocalMoveX(480, 0.75f);
                _playerTwoUI.transform.DOLocalMoveX(-480, 0.75f);
                _normalSetSquares = false;
                ResetBlinkingNbSquares();
            }
            
            else if (_playerTwo.transform.position.x > _camera.transform.position.x && !_normalSetSquares)
            {
                _playerOneUI.transform.DOLocalMoveX(-480, 0.75f);
                _playerTwoUI.transform.DOLocalMoveX(480, 0.75f);
                _normalSetSquares = true;
                ResetBlinkingNbSquares();
            }
        }
        else if (_isPlayerTwoSquareActive && !_isPlayerOneSquareActive)
        {
            if (_playerOne.transform.position.x > _camera.transform.position.x && _normalSetSquares)
            {
                _playerOneUI.transform.DOLocalMoveX(480, 0.75f);
                _playerTwoUI.transform.DOLocalMoveX(-480, 0.75f);
                _normalSetSquares = false;
                ResetBlinkingNbSquares();
            }
            
            else if (_playerOne.transform.position.x < _camera.transform.position.x && !_normalSetSquares)
            {
                _playerOneUI.transform.DOLocalMoveX(-480, 0.75f);
                _playerTwoUI.transform.DOLocalMoveX(480, 0.75f);
                _normalSetSquares = true;
                ResetBlinkingNbSquares();
            }
        }
    }

    public void WarningUIPlayerOne()
    {
        if (!_isWarningPlayerOne)
        {
            _isWarningPlayerOne = true;
            _nbBlinkPlayerOneSquare = _blinkingDuration;
            playerOneUISquare.DOFade(1, 0);
            playerOneUISquare.transform.DOShakeScale(1f, new Vector3(0.02f, 0, 0), 15, 10, true)
                .OnComplete(() =>
                {
                    playerOneUISquare.DOFade(0, 1f).OnComplete(
                        () =>
                        {
                            _isWarningPlayerOne = false;
                        });
                });
        }
    }

    public void WarningUIPlayerTwo()
    {
        if (!_isWarningPlayerTwo)
        {
            _isWarningPlayerTwo = true;
            _nbBlinkPlayerTwoSquare = _blinkingDuration;
            playerTwoUISquare.DOFade(1, 0);
            playerTwoUISquare.transform.DOShakeScale(1f, new Vector3(0.02f, 0, 0), 15, 10, true)
                .OnComplete(() =>
                {
                    playerTwoUISquare.DOFade(0, 1f).OnComplete(
                        () =>
                        {
                            _isWarningPlayerTwo = false;
                        });
                });
        }
    }

    public void OpenControllerPlayerOne()
    {
        UnsetCentralPlayerSquare();
        SetPlayerOneSquare();
        _isCentralSquareActive = false;
        _isPlayerOneSquareActive = true;
        playerOneUISquare.DOFade(0, _blinkingRate);
    }

    public void CloseControllerPlayerOne()
    {
        UnsetPlayerOneSquare();
        _isPlayerOneSquareActive = false;
        if (!_isPlayerTwoSquareActive)
        {
            SetCentralPlayerSquare();
            _isCentralSquareActive = true;
            _twoPlayersUISquare.DOFade(0, _blinkingRate);
        }
    }

    public void OpenTabletPlayerTwo()
    {
        UnsetCentralPlayerSquare();
        SetPlayerTwoSquare();
        _isCentralSquareActive = false;
        _isPlayerTwoSquareActive = true;
        playerTwoUISquare.DOFade(0, _blinkingRate);
    }

    public void CloseTabletPlayerTwo()
    {
        UnsetPlayerTwoSquare();
        _isPlayerTwoSquareActive = false;
        if (!_isPlayerOneSquareActive)
        {
            SetCentralPlayerSquare();
            _isCentralSquareActive = true;
        }
    }

    private void SetCentralPlayerSquare()
    {
        _twoPlayersUISquare.enabled = true;
        _nbBlinkCentralSquare = 0;
        _blinkCentralTimer = 0;
        _twoPlayersUISquare.DOFade(1, 0);
    }

    private void UnsetCentralPlayerSquare()
    {
        _twoPlayersUISquare.enabled = false;
    }

    private void SetPlayerOneSquare()
    {
        _playerOneUI.SetActive(true);
        _nbBlinkPlayerOneSquare = 0;
        _blinkPlayerOneTimer = 0;
        playerOneUISquare.DOFade(1, 0);
    }

    private void UnsetPlayerOneSquare()
    {
        _playerOneUI.SetActive(false);
    }
    
    private void SetPlayerTwoSquare()
    {
        _playerTwoUI.SetActive(true);
        _nbBlinkPlayerTwoSquare = 0;
        _blinkPlayerOneTimer = 0;
        playerTwoUISquare.DOFade(1, 0);
    }

    private void UnsetPlayerTwoSquare()
    {
        _playerTwoUI.SetActive(false);
    }

    private void UnsetAllSquares()
    {
        _twoPlayersUISquare.enabled = false;
        playerOneUISquare.enabled = false;
        playerTwoUISquare.enabled = false;
    }
}
