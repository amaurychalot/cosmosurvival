using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private CharacterController _characterController;
    [SerializeField] private GameManager _gameManager;
    private Animator _animator;

    [SerializeField] private UIManager _uiManager;
    [SerializeField] private GameObject playerGO;
    [SerializeField] private bool isOutline;
    [SerializeField] private float movementSpeed;
    [SerializeField] private float rotationSpeed;
    [SerializeField] private float jumpSpeed;
    [SerializeField] private float gravity;
    [SerializeField] private int playerNb;

    private bool _playerCanMove;
    
    private Vector3 forward;
    private Vector3 right;

    private float _verticalSpeed = 0;
    
    private bool _isUsingTablet;

    void Start()
    {
        _animator = GetComponent<Animator>();
        forward = Camera.main.transform.forward;
        forward.y = 0;
        forward = Vector3.Normalize(forward);
        right = Quaternion.Euler(new Vector3(0, 90, 0)) * forward;
        _isUsingTablet = false;
        _playerCanMove = false;
    }

    public void PlayerCanMove()
    {
        _playerCanMove = true;
    }

    public void PlayerCantMove()
    {
        _playerCanMove = false;
    }

    public bool isPlayerOneInteracting()
    {
        return Input.GetKey(KeyCode.E);
    }

    public bool isPlayerTwoInteracting()
    {
        return Input.GetMouseButton(0);
    }

    private bool TestMovingKeys()
    {
        if (!_playerCanMove)
            return false;

        if (playerNb == 1)
            return Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.Z) || Input.GetKey(KeyCode.S)
                   || Input.GetKey(KeyCode.D);
        else
            return Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow)
                                                       || Input.GetKey(KeyCode.UpArrow) ||
                                                       Input.GetKey(KeyCode.DownArrow);
    }
    private void Update()
    {
        bool isMoving = TestMovingKeys();

        if (isMoving && _isUsingTablet)
        {
            isMoving = false;
            if (playerNb == 1)
                _uiManager.WarningUIPlayerOne();
            if (playerNb == 2)
                _uiManager.WarningUIPlayerTwo();
        }

        if (!isOutline && isMoving)
            Move(new Vector3(0, _verticalSpeed, 0));
        
        else if (!isOutline)
            _characterController.Move(new Vector3(0, _verticalSpeed, 0) * Time.deltaTime);

        if (_characterController.isGrounded)
        {
            if (playerNb == 1 && Input.GetKey(KeyCode.Space))
                _verticalSpeed = jumpSpeed;
            else
                _verticalSpeed = 0;
        }
        
        else
            _verticalSpeed -= gravity * Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.Escape) && playerNb == 1)
        {
            _gameManager.PauseGame();
        }

        if (playerNb == 2 && Input.GetMouseButtonDown(2))
        {
            if (!_isUsingTablet)
            {
                _uiManager.OpenTabletPlayerTwo();
                _isUsingTablet = true;
            }
            else
            {
                _uiManager.CloseTabletPlayerTwo();
                _isUsingTablet = false;
            }
        }
        

        if (playerNb == 1 && Input.GetKeyDown(KeyCode.A))
        {
            if (!_isUsingTablet)
            {
                _uiManager.OpenControllerPlayerOne();
                _isUsingTablet = true;
            }
            else
            {
                _uiManager.CloseControllerPlayerOne();
                _isUsingTablet = false;
            }
        }

        _animator.SetBool("Running", isMoving);
    }

    void Move(Vector3 gravityMove)
    {
        if (playerNb == 1)
            MovePlayer1(gravityMove);
        else
            MovePlayer2(gravityMove);
    }

    void MovePlayer1(Vector3 gravityMove)
    {
        Vector3 rightMovement = right * movementSpeed * Time.deltaTime
                                * (Input.GetKey(KeyCode.Q) ? -1 : Input.GetKey(KeyCode.D) ? 1 : 0);
        Vector3 upMovement = forward * movementSpeed * Time.deltaTime
                             * (Input.GetKey(KeyCode.Z) ? 1 : Input.GetKey(KeyCode.S) ? -1 : 0);

        Vector3 heading = Vector3.Normalize(rightMovement + upMovement);

        playerGO.transform.forward = heading;
        _characterController.Move(heading * movementSpeed * Time.deltaTime + gravityMove * Time.deltaTime);
    }

    void MovePlayer2(Vector3 gravityMove)
    {
        Vector3 rightMovement = right * movementSpeed * Time.deltaTime
                                * (Input.GetKey(KeyCode.LeftArrow) ? -1 : Input.GetKey(KeyCode.RightArrow) ? 1 : 0);
        Vector3 upMovement = forward * movementSpeed * Time.deltaTime
                             * (Input.GetKey(KeyCode.UpArrow) ? 1 : Input.GetKey(KeyCode.DownArrow) ? -1 : 0);

        Vector3 heading = Vector3.Normalize(rightMovement + upMovement);

        playerGO.transform.forward = heading;
        _characterController.Move(heading * movementSpeed * Time.deltaTime + gravityMove * Time.deltaTime);
    }
}
