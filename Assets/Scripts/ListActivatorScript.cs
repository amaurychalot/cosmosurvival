using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListActivatorScript : MonoBehaviour
{
    [SerializeField] private List<DoorManager> _door;
    [SerializeField] private List<AndDoorScript> _andDoor;
    [SerializeField] private List<BridgeController> _bridge;
    [SerializeField] private List<LaserScript> _laser;
    [SerializeField] private List<OrDoorScript> _orDoor;
    [SerializeField] private List<ListActivatorScript> _listActivator;

    [SerializeField] private int _outputType = 1;
    /*
     * 1 Door (Default)
     * 2 AndDoor
     * 3 Bridge
     * 4 Laser
     * 5 OrDoor
     * 6 ListActivator
     */

    private bool _active;

    // Start is called before the first frame update
    void Start()
    {
        _active = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Activate()
    {
        if (_outputType == 1)
            foreach (var door in _door)
                door.Open();
        else if (_outputType == 2)
            foreach (var andDoor in _andDoor)
                andDoor.SetInputOn();
        else if (_outputType == 3)
            foreach(var bridge in _bridge)
                bridge.Open();
        else if (_outputType == 4)
            foreach (var laser in _laser)
                laser.Activate();
        else if (_outputType == 5)
            foreach (var orDoor in _orDoor)
                orDoor.SetInputOn();
        else if (_outputType == 6)
            foreach (var listActivator in _listActivator)
                listActivator.Activate();
        _active = true;
    }

    public void Desactivate()
    {
        if (_outputType == 1)
            foreach (var door in _door)
                door.Close();
        else if (_outputType == 2)
            foreach (var andDoor in _andDoor)
                andDoor.SetInputOff();
        else if (_outputType == 3)
            foreach (var bridge in _bridge)
                bridge.Close();
        else if (_outputType == 4)
            foreach (var laser in _laser)
                laser.Desactivate();
        else if (_outputType == 5)
            foreach (var orDoor in _orDoor)
                orDoor.SetInputOff();
        else if (_outputType == 6)
            foreach (var listActivator in _listActivator)
                listActivator.Desactivate();
        _active = false;
    }
}
