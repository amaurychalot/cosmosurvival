using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class LeverScript : MonoBehaviour
{
    [SerializeField] private Material _desactived;
    [SerializeField] private Material _activated;

    [SerializeField] private PlayerController _player1;
    [SerializeField] private PlayerController _player2;
    [SerializeField] private GameObject _activationIndicator;
    [SerializeField] private GameObject _stick;

    [SerializeField] private float _coolDown = 1.2f;

    //Output
    [SerializeField] private DoorManager _door;
    [SerializeField] private AndDoorScript _andDoor;
    [SerializeField] private BridgeController _bridge;
    [SerializeField] private LaserScript _laser;
    [SerializeField] private OrDoorScript _orDoor;
    [SerializeField] private ListActivatorScript _listActivator;
    [SerializeField] private int _outputType = 1;
    /*
     * 1 Door (Default)
     * 2 AndDoor
     * 3 Bridge
     * 4 Laser
     * 5 OrDoor
     * 6 ListActivator
     */
    
    private float _timer;
    
    private bool _triggerStay1 = false;
    private bool _triggerStay2 = false;

    [SerializeField] private bool _active = false;
    
    //Position du levier par defaut (localPosition)
    private Vector3 _initPosition;
    private Vector3 _initRotation;
        
    void Start()
    {
        _timer = _coolDown;
        _initPosition = _stick.transform.localPosition;
        _initRotation = new Vector3(_stick.transform.localRotation.x, _stick.transform.localRotation.y, _stick.transform.localRotation.z);
    }

    void Update()
    {
        if (_timer >= _coolDown)
        {
            if (_triggerStay1 && _player1.isPlayerOneInteracting())
            {
                if (_active)
                    Desactivate();
                else
                    Activate();
                _timer = 0;
            }

            if (_triggerStay2 && _player2.isPlayerTwoInteracting())
            {
                if (_active)
                    Desactivate();
                else
                    Activate();
                _timer = 0;
            }
        }
        else
        {
            _timer += Time.deltaTime;
        }
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player1"))
            _triggerStay1 = true;
        if (other.CompareTag("Player2"))
            _triggerStay2 = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player1"))
            _triggerStay1 = false;
        if (other.CompareTag("Player2"))
            _triggerStay2 = false;
    }

    private void Activate()
    {
        if (_outputType == 1)
            _door.Open();
        else if (_outputType == 2)
            _andDoor.SetInputOn();
        else if (_outputType == 3)
            _bridge.Open();
        else if (_outputType == 4)
            _laser.Activate();
        else if (_outputType == 5)
            _orDoor.SetInputOn();
        else if (_outputType == 6)
            _listActivator.Activate();
        _active = true;
        _stick.transform.DOLocalMoveZ(_initPosition.z * -1, 1);
        _stick.transform.DOLocalRotate(new Vector3(_initRotation.x * -1, _initRotation.y, _initRotation.z), 1, RotateMode.Fast);
        _activationIndicator.GetComponent<MeshRenderer>().material = _activated;
    }

    private void Desactivate()
    {
        if (_outputType == 1)
            _door.Close();
        else if (_outputType == 2)
            _andDoor.SetInputOff();
        else if (_outputType == 3)
            _bridge.Close();
        else if (_outputType == 4)
            _laser.Desactivate();
        else if (_outputType == 5)
            _orDoor.SetInputOff();
        else if (_outputType == 6)
            _listActivator.Desactivate();
        _active = false;
        _stick.transform.DOLocalMoveZ(_initPosition.z, 1);
        _stick.transform.DOLocalRotate(_initRotation, 1, RotateMode.Fast);
        _activationIndicator.GetComponent<MeshRenderer>().material = _desactived;
    }
}
