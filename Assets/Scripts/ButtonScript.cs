using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonScript : MonoBehaviour
{
    [SerializeField] private Material _desactived;
    [SerializeField] private Material _activated;
    [SerializeField] private PlayerController _player1;
    [SerializeField] private PlayerController _player2;
    
    //Timer
    private float _timer;
    [SerializeField] private float _coolDown = 3f;
    private bool _isActive;
    
    //Output
    [SerializeField] private DoorManager _door;
    [SerializeField] private AndDoorScript _andDoor;
    [SerializeField] private BridgeController _bridge;
    [SerializeField] private LaserScript _laser;
    [SerializeField] private OrDoorScript _orDoor;
    [SerializeField] private ListActivatorScript _listActivator;
    [SerializeField] private int _outputType = 1;
    /*
     * 1 Door (Default)
     * 2 AndDoor
     * 3 Bridge
     * 4 Laser
     * 5 OrDoor
     * 6 ListActivator
     */

    private bool _triggerStay1 = false;
    private bool _triggerStay2 = false;
    
    // Start is called before the first frame update
    void Start()
    {
        _isActive = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (_triggerStay1 && _player1.isPlayerOneInteracting() && !_isActive)
        {
            Activate();
            _timer = 0;
            _isActive = true;
        }

        if (_triggerStay2 && _player2.isPlayerTwoInteracting() && !_isActive)
        {
            Activate();
            _timer = 0;
            _isActive = true;
        }

        if (_isActive)
        {
            _timer += Time.deltaTime;
            if (_timer >= _coolDown)
            {
                Desactivated();
                _isActive = false;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player1"))
            _triggerStay1 = true;
        if (other.CompareTag("Player2"))
            _triggerStay2 = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player1"))
            _triggerStay1 = false;
        if (other.CompareTag("Player2"))
            _triggerStay2 = false;
    }

    private void Activate()
    {
        if (_outputType == 1)
            _door.Open();
        else if (_outputType == 2)
            _andDoor.SetInputOn();
        else if (_outputType == 3)
            _bridge.Open();
        else if (_outputType == 4)
            _laser.Activate();
        else if (_outputType == 5)
            _orDoor.SetInputOn();
        else if (_outputType == 6)
            _listActivator.Activate();
        gameObject.GetComponent<MeshRenderer>().material = _activated;
    }

    private void Desactivated()
    {
        if (_outputType == 1)
            _door.Close();
        else if (_outputType == 2)
            _andDoor.SetInputOff();
        else if (_outputType == 3)
            _bridge.Close();
        else if (_outputType == 4)
            _laser.Desactivate();
        else if (_outputType == 5)
            _orDoor.SetInputOff();
        else if (_outputType == 6)
            _listActivator.Desactivate();
        gameObject.GetComponent<MeshRenderer>().material = _desactived;
    }
}
