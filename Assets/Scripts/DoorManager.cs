using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class DoorManager : MonoBehaviour
{
    private Vector3 _initPosLeft;
    private Vector3 _initPosRight;
    [SerializeField] private GameObject _leftDoor;
    [SerializeField] private GameObject _rightDoor;

    public bool IsActive;
    // Start is called before the first frame update
    void Start()
    {
        _initPosLeft = _leftDoor.transform.position;
        _initPosRight = _rightDoor.transform.position;
        IsActive = false;
    }

    //ouvre la parte en fonction de l'animation
    public void Open()
    {
        //translation du bloque TODO animation
        _leftDoor.transform.DOMoveX(_initPosLeft.x - 4, 1f);
        _rightDoor.transform.DOMoveX(_initPosRight.x + 4, 1f);
        IsActive = true;
    }

    public void Close()
    {
        _leftDoor.transform.DOMoveX(_initPosLeft.x, 1f);
        _rightDoor.transform.DOMoveX(_initPosRight.x, 1f);
        IsActive = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
