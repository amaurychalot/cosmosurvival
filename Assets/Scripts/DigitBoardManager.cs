using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DigitBoardManager : MonoBehaviour
{
    [SerializeField] private Text _dial;

    public void AddOne()
    {
        if (_dial.text.Length < 8)
            _dial.text += "1";
    }
    
    public void AddTwo()
    {
        if (_dial.text.Length < 8)
            _dial.text += "2";
    }

    public void AddThree()
    {
        if (_dial.text.Length < 8)
            _dial.text += "3";
    }

    public void AddFour()
    {
        if (_dial.text.Length < 8)
            _dial.text += "4";
    }

    public void AddFive()
    {
        if (_dial.text.Length < 8)
            _dial.text += "5";
    }

    public void AddSix()
    {
        if (_dial.text.Length < 8)
            _dial.text += "6";
    }

    public void AddSeven()
    {
        if (_dial.text.Length < 8)
            _dial.text += "7";
    }

    public void AddEight()
    {
        if (_dial.text.Length < 8)
            _dial.text += "8";
    }

    public void AddNine()
    {
        if (_dial.text.Length < 8)
            _dial.text += "9";
    }

    public void AddZero()
    {
        if (_dial.text.Length < 8)
            _dial.text += "0";
    }

    public void Erase()
    {
        _dial.text = "";
    }
}
