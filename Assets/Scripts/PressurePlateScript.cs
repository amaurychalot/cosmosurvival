using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressurePlateScript : MonoBehaviour
{
    [SerializeField] private Material _activated;
    [SerializeField] private Material _disactivated;
    [SerializeField] private GameObject _plate;

    [SerializeField] private float _coolDown = 0.5f;

    //Output
    [SerializeField] private DoorManager _door;
    [SerializeField] private AndDoorScript _andDoor;
    [SerializeField] private BridgeController _bridge;
    [SerializeField] private LaserScript _laser;
    [SerializeField] private OrDoorScript _orDoor;
    [SerializeField] private ListActivatorScript _listActivator;
    [SerializeField] private int _outputType = 1;
    /*
     * 1 Door (Default)
     * 2 AndDoor
     * 3 Bridge
     * 4 Laser
     * 5 OrDoor
     * 6 ListActivator
     */

    [SerializeField] private bool _isSpecific = false;
    [SerializeField] private int _specificPlayer = 0;
    
    private float _timer;

    private bool _isDesactivating = false;
    private bool _triggerStay1 = false;
    private bool _triggerStay2 = false;

    private bool _active;

    // Start is called before the first frame update
    void Start()
    {
        _timer = _coolDown;
        _active = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (_isSpecific)
        {
            //TODO si un joueur en particulier doit etre sur la plaque
        }
        else
        {
            if ((_triggerStay1 || _triggerStay2) && !_active)
            {
                Activate();
                _active = true;
            }

            if (!_triggerStay1 && !_triggerStay2 && _active && !_isDesactivating)
            {
                _timer = 0;
                _isDesactivating = true;
            }
        }

        if (_isDesactivating)
        {
            _timer += Time.deltaTime;
            if (_timer >= _coolDown)
            {
                _isDesactivating = false;
                _active = false;
                Desactivate();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player1"))
            _triggerStay1 = true;
        if (other.CompareTag("Player2")) 
            _triggerStay2 = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player1"))
            _triggerStay1 = false;
        if (other.CompareTag("Player2")) 
            _triggerStay2 = false;
    }

    private void Activate()
    {
        if (_outputType == 1)
            _door.Open();
        else if (_outputType == 2)
            _andDoor.SetInputOn();
        else if (_outputType == 3)
            _bridge.Open();
        else if (_outputType == 4)
            _laser.Activate();
        else if (_outputType == 5)
            _orDoor.SetInputOn();
        else if (_outputType == 6)
            _listActivator.Activate();
        _plate.GetComponent<MeshRenderer>().material = _activated;
    }

    private void Desactivate()
    {
        if (_outputType == 1)
            _door.Close();
        else if (_outputType == 2)
            _andDoor.SetInputOff();
        else if (_outputType == 3)
            _bridge.Close();
        else if (_outputType == 4)
            _laser.Desactivate();
        else if (_outputType == 5)
            _orDoor.SetInputOff();
        else if (_outputType == 6)
            _listActivator.Desactivate();
        _plate.GetComponent<MeshRenderer>().material = _disactivated;
    }
}
