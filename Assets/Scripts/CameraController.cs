using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private GameObject _player1;
    [SerializeField] private GameObject _player2;
    
    //Positions maximales de la camera
    [SerializeField] private float _maxUp;
    [SerializeField] private float _maxDown;
    [SerializeField] private float _maxLeft;
    [SerializeField] private float _maxRight;
    [SerializeField] private float _maxFar;
    [SerializeField] private float _maxNear;

    //Distance entre les joueurs et la camera
    [SerializeField] private float _xMaxDist;
    [SerializeField] private float _yMaxDist;
    [SerializeField] private float _zMaxDist;

    private Vector3 _distanceFromPlayers;

    private float _initY;
    private float _initRotateX;
    private int _posIndicator;

    [SerializeField] private float _distOneThreshold;
    [SerializeField] private float _distOnePos;
    [SerializeField] private float _distOneRot;
    [SerializeField] private float _distTwoThreshold;
    [SerializeField] private float _distTwoPos;
    [SerializeField] private float _distTwoRot;

    // Start is called before the first frame update
    void Start()
    {
        Vector3 centralPos = new Vector3(
            (_player1.transform.position.x + _player2.transform.position.x) / 2,
            (_player1.transform.position.y + _player2.transform.position.y) / 2,
            (_player1.transform.position.z + _player2.transform.position.z) / 2);
        _distanceFromPlayers = transform.position - centralPos;
        _initY = transform.position.y;
        _initRotateX = transform.rotation.eulerAngles.x;
        _posIndicator = 0;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 centralPos = new Vector3(
            (_player1.transform.position.x + _player2.transform.position.x) / 2,
            (_player1.transform.position.y + _player2.transform.position.y) / 2,
            (_player1.transform.position.z + _player2.transform.position.z) / 2);

        float _dist = Vector3.Distance(_player1.transform.position, _player2.transform.position);
        
        //Debug.Log(_dist);

        if (Math.Abs(centralPos.x - transform.position.x) > _xMaxDist)
        {
            transform.DOMoveX(centralPos.x + _distanceFromPlayers.x, 0.8f);
        }

        if (Math.Abs(centralPos.z - transform.position.z + _distanceFromPlayers.z) > _zMaxDist)
        {
            transform.DOMoveZ(centralPos.z + _distanceFromPlayers.z, 1f);
        }

        if (_dist > _distOneThreshold && _dist < _distTwoThreshold && _posIndicator != 1)
        {
            transform.DOMoveY(_distOnePos, 0.8f);
            transform.DORotate(new Vector3(_distOneRot, 0, 0), 0.8f);
            _posIndicator = 1;
        }

        if (_dist < _distOneThreshold && _posIndicator != 0)
        {
            transform.DOMoveY(_initY, 1f);
            transform.DORotate(new Vector3(_initRotateX, 0, 0), 0.8f);
            _posIndicator = 0;
        }

        if (_dist > _distTwoThreshold && _posIndicator != 2)
        {
            transform.DOMoveY(_distTwoPos, 1f);
            transform.DORotate(new Vector3(_distTwoRot, 0, 0), 0.8f);
            _posIndicator = 2;
        }
    }
}
