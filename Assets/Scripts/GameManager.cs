using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    //UI
    [SerializeField] private GameObject _gameOverScreen;
    [SerializeField] private GameObject _winningScreen;
    [SerializeField] private GameObject _pauseScreen;
    
    //Players Ref
    [SerializeField] private PlayerController _playerOne;
    [SerializeField] private PlayerController _playerTwo;

    //Game Design
    [SerializeField] private DoorManager _finalDoor;
    private bool _isLevelFinished;
    private float _timerLevelFinished;
    [SerializeField] private float _coolDownLevelFinished = 1f;
    private bool _gamePaused;
    [SerializeField] private int _levelNumber;
    
    //Player Wait before moving
    private float _timerPlayersCanMove;
    private bool _playersCanMove;
    [SerializeField] private float _coolDownPlayersCanMove = 1f;
    
    // Start is called before the first frame update
    void Start()
    {
        _timerPlayersCanMove = 0;
        _playersCanMove = false;
        _isLevelFinished = false;
        _gamePaused = false;
        Time.timeScale = 1f;
    }

    public void KillPlayer1()
    {
        GameOver(0);
        _gameOverScreen.SetActive(true);
    }

    public void KillPlayer2()
    {
        GameOver(0);
        _gameOverScreen.SetActive(true);
    }

    public void GameOver(int type)
    {
        Debug.Log("Game Over");
        _playerOne.PlayerCantMove();
        _playerTwo.PlayerCantMove();
    }

    public void Winning()
    {
        _timerLevelFinished = 0;
        _isLevelFinished = true;
        _finalDoor.Close();
    }

    public void PauseGame()
    {
        if (_gamePaused)
        {
            _pauseScreen.SetActive(false);
            _playerOne.PlayerCanMove();
            _playerTwo.PlayerCanMove();
            _gamePaused = false;
            Time.timeScale = 1f;
        }

        else
        {
            _pauseScreen.SetActive(true);
            _playerOne.PlayerCantMove();
            _playerTwo.PlayerCantMove();
            _gamePaused = true;
            Time.timeScale = 0f;
        }
    }

    public void NextLevel()
    {
        SceneManager.LoadScene("Scenes/Level" + (_levelNumber + 1), LoadSceneMode.Single);
    }
    
    public void Retry()
    {
        SceneManager.LoadScene("Scenes/Level" + _levelNumber, LoadSceneMode.Single);
    }

    public void ReturnToMenu()
    {
        SceneManager.LoadScene("Scenes/Main Menu", LoadSceneMode.Single);
    }

    // Update is called once per frame
    void Update()
    {
        if (!_playersCanMove)
        {
            _timerPlayersCanMove += Time.deltaTime;
            if (_timerPlayersCanMove >= _coolDownPlayersCanMove)
            {
                _playerOne.PlayerCanMove();
                _playerTwo.PlayerCanMove();
                _playersCanMove = true;
            }
        }

        if (_isLevelFinished)
        {
            FinishLevel();
        }
    }

    private void FinishLevel()
    {
        _timerLevelFinished += Time.deltaTime;
        if (_timerLevelFinished >= _coolDownLevelFinished)
        {
            SaveProgression();
            _winningScreen.SetActive(true);
            _playerOne.PlayerCantMove();
            _playerTwo.PlayerCantMove();
        }
    }

    private void SaveProgression()
    {
        DataClass _save = new DataClass();
        _save = (DataClass) Storage.Load("save");
        if (_save._levelProgression < _levelNumber)
        {
            _save._levelProgression = _levelNumber;
        }

        Storage.Save(_save, "save");
    }
}
