using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class BridgeController : MonoBehaviour
{
    [SerializeField] private GameObject _bridge;
    private Vector3 _initPos;
    
    // Start is called before the first frame update
    void Start()
    {
        _initPos = _bridge.transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void Open()
    {
        _bridge.transform.DOLocalMoveX(3.8f, 1.5f);
    }

    public void Close()
    {
        _bridge.transform.DOLocalMoveX(_initPos.x, 1.5f);
    }
}
