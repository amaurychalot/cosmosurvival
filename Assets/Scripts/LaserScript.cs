using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserScript : MonoBehaviour
{
    [SerializeField] private GameObject _laser;
    [SerializeField] private GameManager _gameManager;

    private bool _laserActivated = true;
    private bool _aPlayerGotKilled;
    
    // Start is called before the first frame update
    void Start()
    {
        _aPlayerGotKilled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (_laserActivated)
        {
            if (other.CompareTag("Player1"))
                _gameManager.KillPlayer1();
            
            if (other.CompareTag("Player2"))
                _gameManager.KillPlayer2();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (_laserActivated && !_aPlayerGotKilled)
        {
            if (other.CompareTag("Player1"))
                _gameManager.KillPlayer1();
            
            if (other.CompareTag("Player2"))
                _gameManager.KillPlayer2();

            _aPlayerGotKilled = true;
        }
    }

    public void Activate()
    {
        _laser.SetActive(false);
        _laserActivated = false;
    }

    public void Desactivate()
    {
        _laser.SetActive(true);
        _laserActivated = true;
    }
    
    // Update is called once per frame
    void Update()
    {
        
    }
}
