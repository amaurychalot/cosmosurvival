using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class CharacterSpinScript : MonoBehaviour
{
    [SerializeField] private float _rotateSpeed = 10f;
    [SerializeField] private Vector3 _initPos = new Vector3(1.33000004f, 0.800000012f, -8.85000038f);
    [SerializeField] private Vector3 _finalPos = new Vector3(-1.63f, 0.800000012f, -8.85000038f);
    [SerializeField] private float _duration = 25f;
    
    private bool _resetMove;
    
    // Start is called before the first frame update
    void Start()
    {
        _resetMove = true;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.back, _rotateSpeed * Time.deltaTime, Space.World);

        if (_resetMove)
        {
            _resetMove = false;
            transform.DOMove(_finalPos, _duration, false)
                .OnComplete(() =>
                {
                    transform.position = _initPos;
                    _resetMove = true;
                });
        }

    }
}
