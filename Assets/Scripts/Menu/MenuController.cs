using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    [SerializeField] private GameObject _mainMenu;

    [SerializeField] private GameObject _selectLevelPage1;
    [SerializeField] private GameObject _startButton;
    [SerializeField] private GameObject _continueButton;
    [SerializeField] private List<Button> _levelButtons;

    private DataClass _save;
    
    // Start is called before the first frame update
    void Start()
    {
        _mainMenu.SetActive(true);
        _selectLevelPage1.SetActive(false);
        _save = new DataClass();
        _save._levelProgression = 0;
        if (Storage.DoesFileExist("save"))
        {
            _save = (DataClass) Storage.Load("save");
        }
        else
        {
            Storage.Save(_save, "save");
        }

        if (_save._levelProgression == 0)
        {
            _startButton.SetActive(true);
            _continueButton.SetActive(false);
        }

        else
        {
            _startButton.SetActive(false);
            _continueButton.SetActive(true);
        }
        
        SetLevelButtons();
    }

    private void SetLevelButtons()
    {
        foreach (var b in _levelButtons)
            b.interactable = false;

        for (int i = 0; i <= _save._levelProgression; i++)
        {
            _levelButtons[i].interactable = true;
            //Debug.Log("Level " + i + 1 + " is available");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SelectLevelOne()
    {
        SceneManager.LoadScene("Scenes/Level1", LoadSceneMode.Single);
    }

    public void SelectLevelTwo()
    {
        SceneManager.LoadScene("Scenes/Level2", LoadSceneMode.Single);
    }

    public void EnterSelectLevel()
    {
        _mainMenu.SetActive(false);
        _selectLevelPage1.SetActive(true);
    }

    public void EnterMainMenu()
    {
        _mainMenu.SetActive(true);
        _selectLevelPage1.SetActive(false);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
