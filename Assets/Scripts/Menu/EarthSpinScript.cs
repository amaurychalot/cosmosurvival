using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarthSpinScript : MonoBehaviour
{
    [SerializeField] private float _speed = 10f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.up, _speed * Time.deltaTime, Space.World);
    }
}
