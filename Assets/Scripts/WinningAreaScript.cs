using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinningAreaScript : MonoBehaviour
{
    private bool _triggerStay1;
    private bool _triggerStay2;
    private bool _levelFinished;

    [SerializeField] private GameManager _gameManager;
    
    // Start is called before the first frame update
    void Start()
    {
        _triggerStay1 = false;
        _triggerStay2 = false;
        _levelFinished = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (_triggerStay1 && _triggerStay2 && !_levelFinished)
        {
            _levelFinished = true;
            _gameManager.Winning();
        }
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player1"))
            _triggerStay1 = true;
        if (other.CompareTag("Player2")) 
            _triggerStay2 = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player1"))
            _triggerStay1 = false;
        if (other.CompareTag("Player2")) 
            _triggerStay2 = false;
    }
}
