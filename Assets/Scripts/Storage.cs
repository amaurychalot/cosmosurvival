using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class Storage
{
    public static void Save(object entity, string fileName)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream stream = File.Create(Application.persistentDataPath + "/" + fileName);
        formatter.Serialize(stream, entity);
        stream.Close();
    }

    public static object Load(string fileName)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream stream = File.Open(Application.persistentDataPath + "/" + fileName, FileMode.Open);
        var entity = formatter.Deserialize(stream);
        stream.Close();
        return entity;
    }

    public static bool DoesFileExist(string fileName)
    {
        return File.Exists(Application.persistentDataPath + "/" + fileName);
    }
}

[Serializable]
public class DataClass
{
    public int _levelProgression;
}